* vcvs_sat
* saturating vcvs
.subckt vcvs_sat vop von vip vin PARAMS: gain=1 vasat=1 vmid=0
B1 vop von V=vmid+vasat*tanh(gain*(V(vip)-V(vin)))
.ends

* vccs_sat
* saturating vccs
.subckt vccs_sat iop ion vip vin PARAMS: gain=1 iasat=1 imid=0
B1 iop ion I=imid+iasat*tanh(gain*(V(vip)-V(vin)))
.ends

* ccvs_sat
* saturating ccvs
.subckt ccvs_sat vop von iip iin PARAMS: gain=1 vasat=1 vmid=0
V1 iip iin dc 0
B1 vop von V=vmid+vasat*tanh(gain*I(V1))
.ends

* cccs_sat
* saturating cccs
.subckt cccs_sat iop ion iip iin PARAMS: gain=1 iasat=1 imid=0
V1 iip iin dc 0
B1 iop ion I=imid+iasat*tanh(gain*I(V1))
.ends

* ccvs
.subckt ccvs vop von iip iin PARAMS: gain=1
V1 iip iin dc 0
H1 vop von V1 { gain }
.ends

* cccs
.subckt cccs iop ion iip iin PARAMS: gain=1
V1 iip iin dc 0
F1 iop ion V1 { gain }
.ends

* vcsw
.subckt vcsw swtop swbot vip vin PARAMS: ron=1m roff=1meg
S1 swtop swbot vip vin vswitch OFF
.model vswitch sw vt=0 vh=0 ron=1u roff=1e12
.ends

* ccsw
.subckt ccsw swtop swbot iip iin PARAMS: ron=1m roff=1meg
V1 iip iin dc 0
W1 swtop swbot V1 cswitch OFF
.model cswitch csw it=0 ih=0 ron=1u roff=1e12
.ends

* vco
.subckt vco vop von vip vin PARAMS: fc=1 voffset=0 vamplitude=1 gain=1
E1 vctrl 0 vip vin 1
B1 vop von V=voffset+vamplitude*sin( 2*pi*( fc+gain*V(vctrl) )*time )
.ends

* vam
.subckt vam vop von vip vin PARAMS: fc=1 voffset=0 vgain=1
B1 vop von V=voffset+vgain*(V(vip)-V(vin))*sin( 2*pi*fc*time )
.ends

* vpm
.subckt vpm vop von vip vin PARAMS: fc=1 voffset=0 vamplitude=1 gain=1
E1 vctrl 0 vip vin 1
B1 vop von V=voffset+vamplitude*sin( 2*pi*fc*time + gain*V(vctrl)*pi/180 )
.ends

