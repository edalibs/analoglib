* single-ended to differential voltage buffer
.subckt sev2diffv vi vc vop von PARAMS: gain=1
EP vop vc vi vc { 0.5*gain}
EN von vc vi vc {-0.5*gain}
.ends

* opamp
* basic ideal opamp model
* this is an ideal opamp with infinite bandwidth
.subckt opamp vip vin vdd vss vo PARAMS: vgain=1000 ro=1m
BVG_SAT1 v1 vss V=(V(vdd)-V(vss))*(0.5+0.5*tanh({vgain}/(V(vdd)-V(vss))*(V(vip)-V(vin))))
RSUPPLY vdd vss 100meg
*RINDIFF vip vin 100meg
R1 vss v1 1
ROUT v1 vo {ro}
.ends

* opamp2
* basic 2 pole, 1 zero o/p pole compensated opamp model
*
* the transition frequency will be slightly lower than ft since the opamp is
* compensated such that the dominant pole roll-off hits the second pole at ft.
* the zero is at k*ft
.subckt opamp2 vip vin vdd vss vo PARAMS: vgain=1000 ro=10k ft=100meg k=2
RSUPPLY vdd vss 100meg
*RINDIFF vip vin 100meg
.func cap(f) { 1/(2*3.1415*f) }
.func rc(ro, vgain) { ro/(vgain*k-1) }
.func cc(ro, vgain, ft, k) { (vgain*k-1)/(k*2*3.1415*ft*ro) }
E1 vi1 0 vip vin 1
Rp2 vi1 vp2 1
Cp2 vp2 0 { cap(ft) }
BVG_SAT1 vo1 vss V=(V(vdd)-V(vss))*(0.5+0.5*tanh({vgain}/(V(vdd)-V(vss))*V(vp2)))
ROUT vo1 vo {ro}
RC vo v1 { rc(ro, vgain) }
CC v1 vss { cc(ro, vgain, ft, k) }
.ends

* voltage ramp generator (vss to vdd)
.subckt vramp vdd vss vo PARAMS: freq=1 fmm=100
RSUPPLY vdd vss 100meg
.func period(f) { 1/f }
.func ru(f) { 0.99*1/f }
.func rd(f) { 0.01*1/f }
.func cap(k, f) { 1/(k*2*3.1415*f) }
V1 vi 0 dc 0 pulse(0 1 0 { ru(freq) } { rd(freq) } 0 { period(freq) } )
B1 v1 0 V=(V(vdd)-V(vss))*V(vi)
R1 v1 vp1 1
C1 vp1 0 { cap(fmm, freq) }
E1 vo1 vss vp1 0 1
V2 vo vo1 dc 0 ac 1
.ends
